import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Bank b = new Bank();
		Scanner sc = new Scanner(System.in);

		for (int i = 0; i < 10000; i++) {
			if (i % 2 == 0) {
				b.subFromAccount(5);
			} else {
				b.addToAccount(5);
			}
		}
		while (sc.hasNextLine()) {
			String line = sc.nextLine();
			String[] splits = line.split(" ");
			if (splits[0].equals("add")) {
				b.addToAccount(Double.parseDouble(splits[1]));
			} else if (splits[0].equals("sub")) {
				b.subFromAccount(Double.parseDouble(splits[1]));
			} else if (splits[0].equals("balance")) {
				b.balance();
			}
		}
	}

}
