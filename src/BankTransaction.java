import java.util.Random;

public class BankTransaction implements Runnable {

    private TransactionType type;
    private double amount;
    private BankAccount account;

    public BankTransaction(BankAccount account, TransactionType type, double amount) {
        super();
        this.account = account;
        this.type = type;
        this.amount = amount;
    }

    @Override
    public void run() {
        try {
//            Thread.sleep(1L);
            switch (type) {

                case Add:
                    account.addToAccount(amount);
                    break;
                case Sub:
                    account.subFromAccount(amount);
                    break;
            }
        } catch (Exception e) {
            // TODO: handle exception
        }
    }
}
