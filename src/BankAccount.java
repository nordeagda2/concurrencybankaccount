
public class BankAccount {
    private double balance = 0.0;

    public void subFromAccount(double howMuch) {
        balance -= howMuch;
    }

    public void addToAccount(double howMuch) {
        balance += howMuch;
    }

    public void balance() {
        System.out.println("Aktualnie na koncie jest : " + balance);
    }
}
