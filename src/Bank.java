import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Bank {

    private BankAccount account = new BankAccount();
    private ExecutorService service = Executors.newFixedThreadPool(5);

    public void subFromAccount(double howMuch) {
        service.submit(new BankTransaction(account, TransactionType.Sub, howMuch));
    }

    public void addToAccount(double howMuch) {
        service.submit(new BankTransaction(account, TransactionType.Add, howMuch));
    }

    public void balance() {
        account.balance();
    }
}
